# Grant-Request Form

This form was created for UEMF's Research Funding team

* Suzanne Araujo (saraujo@lifespan.org)
* Amy Michaluk (amichaluk@lifespan.org)

The intent of the form is to provide users with a clean interface where they can submit requests for funding for research projects.

This repository holds all of the files required for the FRONT-END of the form.
The front-end at the end will submit a JSON file containing all of the form data.

Upon cloning this repository, you must install all dependencies with the following commands.

    npm install
    bower install
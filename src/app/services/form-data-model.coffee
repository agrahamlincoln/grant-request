app = angular.module "grantRequest"

app.service 'formDataModel', () ->

  formDataModel = this

  formDataModel.data = (
    principalInvestigator: (
      name: ''
      email: ''
      fedId: ''
      vacation: false
    )
    fundOpportunity: (
      sponsor: ''
      fundMech: 'grant'
      details: ''
      website: ''
      dueDate: ''
    )
    proposal: (
      title: ''
      shortTitle: ''
      startDate: ''
      endDate: ''
    )
    special: (
      humans: false
      clinical: false
      phase3: false
      vertebrate: false
      agents: false
      stemcells: false
    )
    personnel:
      [{
        name: ''
        role: 'Principal Investigator'
        effort: ''
      }]
    consultants:
      [{
        name: ''
        email: ''
        fee: ''
      }]
    subawards:
      [{
        name: ''
        principalInvestigator: {
          name: ''
          email: ''
        }
        grantAdmin: {
          name: ''
          email: ''
        }
      }]
    comments: ''
  )
  formDataModel.addConsultant = () ->
    this.data.consultants.push(name: "", email: "", fee: "")

  formDataModel.addPersonnel = () ->
    this.data.personnel.push(name: "", role: "Co-Investigator", effort: "")

  formDataModel.addSubaward = () ->
    this.data.subawards.push(
      name: "",
      principalInvestigator: {
        name: "",
        email: ""
        },
      grantAdmin: {
        name: "",
        email: ""
        }
    )

  return this
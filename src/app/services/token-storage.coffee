app = angular.module "grantRequest"

app.factory 'tokenStorage', ($window) ->
  tokenStorage = {}

  tokenStorage.isValidToken = ->
    getTokenType = typeof($window.localStorage['jwtToken'])
    if typeof($window.localStorage['jwtToken']) == "undefined"
      console.log "token is undefined"
      #token is undefined
      return false
    else
      #a token is saved on the client
      currentTime = new Date().getTime() / 1000
      tokenExpirationType = typeof($window.localStorage['jwtExpiration'])
      if typeof($window.localStorage['jwtExpiration']) == "undefined"
        #token is undefined
        return false
      else
        if currentTime > $window.localStorage['jwtExpiration']
          #token is expired
          return false
        else
          #token is not expired
          return true

  tokenStorage.saveToken = (token) ->
    $window.localStorage['jwtToken'] = token
    $window.localStorage['jwtExpiration'] = (new Date().getTime() / 1000) + 600

  tokenStorage.getToken = ->
    $window.localStorage['jwtToken']

  tokenStorage.tokenExpiration = ->
    $window.localStorage['jwtExpiration']

  tokenStorage.deleteToken = ->
    $window.localStorage.removeItem['jwtToken']
    $window.localStorage.removeItem['jwtExpiration']

  return tokenStorage
    app = angular.module "grantRequest"

    app.service 'httpService', ($http, $q, tokenStorage, formDataModel) ->

      httpService = this

      #load the config file which stores our API Base URL
      $http.get('config.js').then(((res)->
        httpService.baseUrl = res.data.api_base_url
      ), (error)->
        console.log "no config.js found, resorting to relative paths"
        httpService.baseUrl = ".."
      )

      httpService.submitRequest = (data) ->
        return post('/gr/submit/', data)
      httpService.getRequest = (id) ->
        return get('/gr/get/', id)
      httpService.sendRequest = (id) ->
        return get('/gr/send/', id)


      httpService.register = () ->
        deferred = $q.defer()
        promise = deferred.promise

        #prepare the HTTP Request
        data = {
          'email': formDataModel.data.principalInvestigator.email
          'name': formDataModel.data.principalInvestigator.name
        }
        url = httpService.baseUrl + '/register'

        $http.post(url, data).then(((res) ->
          console.log "register(): Received response from server"
          if res.data.success
            tokenStorage.saveToken(res.data.jwt)
            console.log "register(): Token was successfully saved"

            deferred.resolve res.data.jwt
          else
            deferred.reject "Either the server response was not valid JSON, or the server responded with 'no success'"
        ), (error)->
          deferred.reject "Server responded with the status code " + error.status
        )

        return promise

      get = (apiRoute, id) ->
        if typeof httpService.baseUrl == "string"
          url = httpService.baseUrl + apiRoute + id
          #pre-create the http request
          request =
            'method': 'get'
            'url': url

          getAuthHeader().then(((authHeader) ->
            request.headers = {}
            request.headers.Authorization = authHeader
            #submit the http request with the auth header
            return $http request
          ), (error) ->
            #submit the http request without auth headers
            return $http request
          )
        else
          #baseUrl is not defined
          return $q (resolve, reject) ->
            reject "Base URL is not defined"
            return

      post = (apiRoute, data) ->
        if typeof httpService.baseUrl == "string"
          url = httpService.baseUrl + apiRoute
          #pre-create the http request
          request =
            'method': "post"
            'url': url
            'data': data

          getAuthHeader().then(((authHeader)->
            request.headers = {}
            request.headers.Authorization = authHeader
            #submit the http request with the auth header
            return $http request
          ), (error)->
            #submit the http request without the auth header
            return $http request
          )
        else
          #baseUrl is not defined
          return $q (resolve, reject) ->
            reject "Base URL is not defined"
            return

      getAuthHeader = () ->
        #console.log "Retrieving the auth header"

        deferred = $q.defer()
        promise = deferred.promise

        if tokenStorage.isValidToken()
          #console.log "getAuthHeader(): Token is valid, returning current token."
          deferred.resolve "Bearer " + tokenStorage.getToken()
        else
          #console.log "getAuthHeader(): Token is invalid, registering new token."
          httpService.register().then(((token)->
            deferred.resolve "Bearer " + tokenStorage.getToken()
          ), (error)->
            deferred.reject error
          )
        return promise

      return httpService
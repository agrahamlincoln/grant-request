app = angular.module 'grantRequest', [
  'ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize'
  'ngResource', 'ui.router', 'mm.foundation', 'ngMessages'
  'monospaced.elastic' ]
  .config ($stateProvider, $urlRouterProvider) ->
    $stateProvider
      .state "home",
        url: "/"
        templateUrl: "app/views/main.html"
        controller: "MainController"

    $urlRouterProvider.otherwise '/'

#initialize foundation
app.run ($rootScope) ->
  $rootScope.$apply $(document).foundation()
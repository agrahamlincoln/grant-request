app = angular.module "grantRequest"
  
app.controller "MainController", ($scope, $state, $http, $location, $sce, $modal, formDataModel, httpService) ->
    
  #the user attempted to submit the form
  $scope.submitted = false

  #whether the form actually is in the database successfully or not
  $scope.posted = false
  
  $scope.visibility =
    (
      consultants: true
      consultants_tooltip: false
      subawards: true
      subawards_tooltip: false
    )
  
  $scope.data = formDataModel.data
  $scope.model = formDataModel

  $scope.specialDescription = ""

  $scope.result =
    (
      add: (
        success: 0
      )
      send: (
        success: 0
        message: ""
      )
      logheader: ""
      log: ""
      id: -1
    )

  #repeater section methods
  $scope.removeLast = (array) ->
    if array.length > 1
      array.pop()

  $scope.removeRow = (array, index) ->
    if index isnt 0
      array.splice(index, 1)

  $scope.removeAll = (array) ->
    length = array.length
    if length > 1
      array.splice(1, length-1)
  #END repeater section methods


  $scope.confirm = (form) ->
    $scope.submitted = true
    if form.$valid
      return $modal.open(
        templateUrl: 'app/views/preview.html'
        controller: 'previewModalCtrl'
        resolve:
          model: (formDataModel) ->
            formDataModel.data
          form: ->
            form
          result: ->
            $scope.result
          submitted: ->
            true
      ).result.then(((result) ->
        $scope.result = result
        $scope.submitted = true
        $scope.posted = true
        return
      ), (error) ->
        if !$scope.posted
          $scope.submitted = false
        console.log 'Dismissed modal'
      )
    return



  $scope.toggleVisibility = (section) ->
    if (section == "consultants")
      visible = $scope.visibility.consultants
      $scope.visibility.consultants = !visible

      #reset the consultants section
      formDataModel.data.consultants = []
      #re-initialize consultants section
      formDataModel.addConsultant()

    if (section == "consultants_tooltip")
      visible = $scope.visibility.consultants_tooltip
      $scope.visibility.consultants_tooltip = !visible

    if (section == "subawards")
      visible = $scope.visibility.subawards
      $scope.visibility.subawards = !visible

      #reset the subawards section
      formDataModel.data.subawards = []
      #re-initialize subawards
      formDataModel.addSubaward()

    if (section == "subawards_tooltip")
      visible = $scope.visibility.subawards_tooltip
      $scope.visibility.subawards_tooltip = !visible
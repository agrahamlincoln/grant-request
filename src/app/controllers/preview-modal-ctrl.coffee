angular.module "grantRequest"
  .controller 'previewModalCtrl', ($scope, $sce, httpService, model, form, result, submitted) ->
    $scope.data = model
    $scope.form = form
    $scope.result = result
    $scope.submitted = submitted

    $scope.submit = () ->
      if $scope.form.$valid
        return httpService.submitRequest($scope.data).then(((res) ->
          #HTTP Reponse 200-299
          if typeof res.data == 'object'
            $scope.result.add.success = true

            if res.data.Warnings
              $scope.result.logheader = 'Warnings'
              $scope.result.log = $sce.trustAsHtml(res.Warnings)

            $scope.result.id = res.data.request_id

            #since its succesfully added, send the email
            $scope.send()
          else
            #the server response was not valid
            $scope.result.add.success = false
            $scope.result.logheader = 'Server Response'
            $scope.result.log = $sce.trustAsHtml(res.data)
            $scope.result.id = -1

            #close the modal, user will need to submit again
            $scope.$close $scope.result

        ), (err) ->
          console.log "Typeof err"
          console.log typeof err
          if typeof err == 'object'
            if err.data.message == 'Error with your token: Expired token'
              httpService.register().then (->
                console.log "Re-registered token, re-submitting"
                $scope.submit form
              ), (error) ->
                $scope.result.add.success = false
                $scope.result.logheader = 'Authentication Error'
                $scope.result.log = error

            else if $scope.result.message
              $scope.result.add.success = false
              $scope.result.logheader = 'Server Response'
              $scope.result.log = $sce.trustAsHtml(err.data.message)
            $scope.result.success = err.data.success
            if err.data.Errors
              $scope.result.logheader = 'Errors'
              $scope.result.log = $sce.trustAsHtml(err.data.Errors)
            else
              $scope.result.logheader = 'Server Response'
              $scope.result.log = $sce.trustAsHtml(err.data)
          else if typeof err == 'string'
            $scope.result.add.success = false
            $scope.result.logheader = 'Server Response'
            $scope.result.log = $sce.trustAsHtml(err)
          else
            $scope.result.add.success = false
            $scope.result.logheader = 'Server Response'
            $scope.result.log = $sce.trustAsHtml(err.data)
          $scope.result.id = -1

          #Closing modal returning result
          $scope.$close $scope.result
        )

    $scope.send = () ->
      if ($scope.result.id)
        $scope.result.send.message = "Sending..."
      else if ($scope.result.id < 0)
        $scope.result.send.success = false
        $scope.result.send.message = "Error: The request has not yet been submitted, Please press 'Cancel' and attempt to submit your request again."
      else
        $scope.result.send.success = false
        $scope.result.send.message = "There was no request to send."

      if $scope.result.id
        httpService.sendRequest($scope.result.id).then ((res) ->
          if typeof res.data == 'object'
            $scope.result.send.success = res.data.success
            $scope.result.send.message = res.data.message
          else
            console.log 'Server response was in an unexpected format.'
            console.log 'Server Response: ' + res.data
            $scope.result.send.success = false
            $scope.result.send.message = res.data

          #Finished sending send request, closing modal
          $scope.$close($scope.result)
          
        ), (err) ->
          if typeof err.data == 'object'
            $scope.result.send.success = err.data.success
            $scope.result.send.message = err.data.message
          else
            console.log 'Server response was in an unexpected format.'
            console.log 'Server Response: ' + err.data
            $scope.result.send.success = false
            $scope.result.send.message = 'Something went wrong while trying to send your request to the Grants Administrator.'

          #Closing modal, returning result
          $scope.$close($scope.result)
          
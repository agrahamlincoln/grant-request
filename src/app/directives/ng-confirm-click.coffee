angular.module "grantRequest"
  .directive "ngConfirmClick", () ->
    return (
      link: (scope, element, attr) ->
        msg = attr.ngConfirmClick or "Are you sure?"
        clickAction = attr.ngClick
        attr.ngClick = ""
        
        element.bind('click', (event) ->
          if window.confirm(msg)
            scope.$eval(clickAction)
        )
    )
angular.module "grantRequest"
  .directive "combobox", () ->
    transclude: true
    scope:
      index: '='
    require: "ngModel"
    template: '<div class="wrapper-dropdown">' +
              '<div class="arrow-container" ng-if="index>0" ng-click="toggleDropDown()"><div class="arrow right"></div></div>' +
              '<input type="text" ng-disabled="index==0"></input>' +
              '<ng-transclude ng-if="index > 0"></ng-transclude></div>'
    link: (scope, element, attrs, ngModelController, transclude) ->
      ngModelController.$render = () ->
        templateText = element.find('input')
        templateText.val(ngModelController.$viewValue)

      updateModel = (newval) ->
        ngModelController.$setViewValue(newval)
        ngModelController.$render()
        return false

      scope.changeInput = (newval)->
        updateModel(newval)

      scope.toggleDropDown = () ->
        list = element.find('ul')
        button = element.find('div')[2]
        if $(button).hasClass('down')
          $(button).removeClass('down')
          $(button).addClass('right')
        else if $(button).hasClass('right')
          $(button).removeClass('right')
          $(button).addClass('down')
        $(list).toggleClass('active')
        false

      #event listener for when the user clicks away from the dropdown
      document.addEventListener "click", (e) ->
        if e.target.classList.contains("arrow") or e.target.classList.contains("arrow-container")
        else
          uls = document.querySelectorAll("ul.dropdown")
          arrows = document.querySelectorAll("div.arrow")

          for i in [0...uls.length]
            uls[i].classList.remove("active")

          for i in [0...arrows.length]
            arrows[i].classList.remove('down')
            arrows[i].classList.add('right')
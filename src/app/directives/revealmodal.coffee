app = angular.module 'grantRequest'

app.directive 'revealModal', () ->
  (scope, elem, attrs) ->
    scope.$watch attrs.revealModal, (val) ->
      if val
        elem.foundation 'reveal', 'open'
      else
        elem.foundation 'reveal', 'close'
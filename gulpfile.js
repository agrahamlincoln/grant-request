'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var wrench = require('wrench');
var jade = require('gulp-jade');

var options = {
  src: 'src',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e',
  errorHandler: function(title) {
    return function(err) {
      gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
      this.emit('end');
    };
  },
  wiredep: {
    directory: 'bower_components',
    exclude: [/foundation\.css/]
  }
};

wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file)(options);
});

gulp.src('./scss/*.scss')
  .pipe(sass({
    includePaths:['bower_components/foundation/scss']
  }))
  .pipe(gulp.dest('./css'));

gulp.task('sass', function() {
  gulp.src('./styles/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function() {
  gulp.watch('.styles/**/*.scss', ['sass']);
});

gulp.task('default', ['clean'], function () {
    gulp.start('build');
});

gulp.task('templates', function () {
  
  gulp.src('./src/*.jade')
    .pipe(jade({
      
    }))
    
    .pipe(gulp.dest('./dist/'))
});